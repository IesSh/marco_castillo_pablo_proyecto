<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;


    protected $fillable =
    [
        'dni',
        'nombre',
        'apellido1',
        'apellido2',
        'telefono',
        'especialidad'
    ];

    
    public function patients()
    {
        return $this->hasMany(Patient::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable=[

        'survey_id',
        'question',

    ];
    //una pregunta pertenece a un cuestionario
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    } 
    //una pregunta tiene muchas respuestas
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }     
}

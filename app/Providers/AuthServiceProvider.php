<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //añadimos las políticas que hay en nuestra aplicación
        'App\Models\Doctor' => 'App\Policies\DoctorPolicy',
        'App\Models\Patient' => 'App\Policies\PatientPolicy',
        'App\Models\Survey' => 'App\Policies\SurveyPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

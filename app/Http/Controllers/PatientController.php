<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Patient;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre = $request->nombre;
        $query = Patient::query();

        if ($nombre) {
            $query->where('nombre', 'like',  "%$nombre%");
        }

        
        $patients = $query->paginate(15);

        $patients->withPath("/patients?nombre=$nombre");

        return view('patient.index', [
            'patients' => $patients,
            'nombre' => $nombre,
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Patient::class);
        return view ('patient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'dni' => 'required|unique:patients|max:9',
            'nombre' => 'required|string|max:255',
            'apellido1' => 'required|string|max:255',
            'apellido2' => 'required|string|max:255',
            'sexo' => 'in:H,M',
            'fechaNacimiento' => 'required|string|max:255',
            'direccion' => 'required|string|max:255',
            'poblacion' => 'required|string|max:255',
            'provincia' => 'required|string|max:255',
            'CP' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            
        ];

        $request->validate($rules);
        
        $patient=Patient::create($request->all());

        return redirect('/patients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return view('patient.show', ['patient' =>$patient]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        $this->authorize('update', $patient);
        return view('patient.edit', ['patient' => $patient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        $rules = [
            'dni' => 'required|max:9|unique:patients,dni, ' . $patient->id,
            'nombre' => 'required|string|max:255',
            'apellido1' => 'required|string|max:255',
            'apellido2' => 'required|string|max:255',
            'sexo' => 'in:H,M',
            'fechaNacimiento' => 'required|string|max:255',
            'direccion' => 'required|string|max:255',
            'poblacion' => 'required|string|max:255',
            'provincia' => 'required|string|max:255',
            'CP' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            
        ];

        $request->validate($rules);
        $patient->fill($request->all());

        $patient->save();
        return redirect('/patients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $this->authorize('delete', $patient);
        $patient->delete();
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\QuestionnairesExport;
use App\Models\Survey;
use App\Models\Questionnaire;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class QuestionnaireController extends Controller
{

    public function index ()
    {
        $questionnaires = Questionnaire::all();

        return view('questionnaire.index', ['questionnaires' => $questionnaires]);
        

    }
    public function export()
    {
        return Excel::download(new QuestionnairesExport,'questionnaires.xls');
    }

    
    public function store(Request $request, Survey $survey)
    {
        //dd(request()->all());

        /*$data=request()->validate([
            'responses.*.answer_id'=>'required',
            'responses.*.question_id'=>'required',
            'questionnaire.name'=>'required',

        ]);

        $questionnaire = $survey->questionnaires()->create($data);
        $questionnaire->responses()->createMany($data['responses']);       
        

        return redirect('/patients');*/

        $data = request()->validate([
            'responses.*.answer_id'=>'required',
            'responses.*.question_id'=>'required',
            'questionnaire.name'=>'required',
                        
        ]);

    

        
        $questionnaire=$survey->questionnaires()->create($data['questionnaire']);
        $questionnaire->responses()->createMany($data['responses']);
        

        //return ('GRACIAS');
        return view('questionnaire.index', ['questionnaire'=>$questionnaire]);


    }
    //como segundo argumento usamos el slug que proviene de la ruta
    public function show (Survey $survey, $slug)
    {       //lazy load preguntas en mis respuestas
            $survey->load('questions.answers');

            return view ('questionnaire.show', compact('survey'));

    }
}

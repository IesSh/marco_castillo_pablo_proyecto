<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;
use App\Models\Survey;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Survey $survey)
    {
        return view ('question.create', compact('survey'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Survey $survey)
    {
       /* $rules = [
            'question' => 'required|string|max:255',                                   
        ];

        $request->validate($rules);
        $all = $request->all();
       $question=Question::create($all);        

        return redirect('/questions');*/

        $data=request()->validate([

            //viene del array en la vista create: question[question]
                'question.question'=>'required', 
            //viene del array en la vista create: answers[][answer]   
                'answers.*.answer'=>'required',        
        ]);

        //dd($data);
            //usamos el survey que tenemos de parámetro y cogemos questions de la relación creada entre 
            //tablas y creamos una nueva question.Esto me devuelve el question model que guardamos
            //en una variable que usamos para las respuestas tb
        $question=$survey->questions()->create($data['question']);
        //You may use the createMany method to create multiple related models:
        $question->answers()->createMany($data['answers']);

        return redirect('/surveys/'.$survey->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::where('survey_id',$id)->first();
        return view('question.show',['question' => $question]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey, Question $question)
    {
        $question->answers()->delete();

        $question->delete();

        return redirect('/surveys/'.$survey->id);
    }
}

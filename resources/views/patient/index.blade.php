@extends('layouts.app')

<style>
    table {
        display: block;
        overflow-x: auto;
    }

    .static {
        position: absolute;
        background: #fff;
        

    }

    .first-col {
        padding-left: 120px !important;
    }

    
</style>

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div class="card border-secondary mt-5">
                <div class="class-header m-5">
                <h3>
                    <i class="fas fa-procedures ml-5"> PACIENTES </i>
                    @can('create', \App\Models\Patient::class)
                    <a class="btn btn-secondary btn-sm ml-3" href="/patients/create">Nuevo</a>
                    @endcan

                </h3>
            </div>
        </div>

            <div class="card-header m-5">
                <form action="/patients" method="get">
                    <th><input class="form-control" type="text" placeholder="ingresa solo el nombre por favor " name="nombre" value="{{$nombre}}"></th>
                    <th><input class="btn text-white mt-3" style="background:#e58c8a;" type="submit" value="Filtrar"></th>
                </form>

            </div>

            <table class="table table-hover table-responsive">
                <thead>
                    <tr>
                        <th class="static" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">DNI</th>
                        <th  class="first-col" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black ">Nombre</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Apellido1</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Apellido2</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Sexo</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >FechaNacimiento</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Dirección</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Población</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Provincia</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >CP</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black" >Teléfono</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($patients as $patient)
                    <tr>
                        <td class="static">{{$patient->dni}} </td>
                        <td class="first-col">{{$patient->nombre}} </td>
                        <td>{{$patient->apellido1}} </td>
                        <td>{{$patient->apellido2}} </td>
                        <td>{{$patient->sexo}} </td>
                        <td>{{$patient->fechaNacimiento}} </td>
                        <td>{{$patient->direccion}} </td>
                        <td>{{$patient->poblacion}} </td>
                        <td>{{$patient->provincia}} </td>
                        <td>{{$patient->CP}} </td>
                        <td>{{$patient->telefono}} </td>

                        <td> <a class="btn-sm text-white" style="background:#e58c8a;" btn-sm float-right" href="/patients/{{$patient->id}}"><i class="far fa-eye"></a></td>
                        @can('update', $patient)
                        <td> <a class="btn-sm text-white" style="background:#e58c8a;" btn-sm float-right" href="/patients/{{$patient->id}}/edit"><i class="far fa-edit"></a></td>
                        @endcan
                        @can('delete', $patient)
                        <td>
                            <form action="/patients/{{$patient->id}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input class="btn btn-danger btn-sm float-right" type="submit" value="Borrar">
                        </td>
                        @endcan
                        </form>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">No hay pacientes registrados con ese nombre</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {!! $patients->links() !!}
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<style>
    h1 {
        text-align: center;
    }
</style>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-12 col-xl-12">
            <div class="card mt-5 mb-5">

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <table class="table table-responsive">

                        <tbody>

                            <tr>
                                <td><img class="img-fluid rounded mt-3" src="../images/survey.jpg" width="350px" height="250px"></td>


                                <td>
                                    <div class="card border-secondary m-2">
                                        <div class="class-header m-3">

                                            <h3 class="text-center"> <i class=" fas fa-user-circle fa-lg float-right"> </i><span> {{ __('¡ Bienvenida  ') }}{{ Auth::user()->name  }} ! </span></h3>
                                        </div>
                                    </div>

                                    <div class="card border-secondary m-2">
                                        <div class="class-header m-3">
                                            <h4 class="text-center"><span>Cuestionarios disponibles en estos momentos </span></h4>
                                        </div>


                                        <table class="table table-responsive table-hover ">
                                            <thead>
                                                <tr style="text-align:center">
                                                    <th scope="col"></th>
                                                    <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Título</th>
                                                    <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Descripción</th>
                                                    <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">URL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($surveys as $key=>$survey)
                                                <tr style="text-align:center">
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{$survey->title}}</td>
                                                    <td>{{$survey->description}}</td>
                                                    <td><a href="/questionnaires/{{$survey->id}}-{{Str::slug($survey->title)}}">{{$survey->title}}</a></td>

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>

                                </td>
                            </tr>


                        </tbody>
                    </table>



                </div>

            </div>

            @endsection

            @section('footer')

            @endsection
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="http://fonts.cdnfonts.com/css/exmouth" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">



</head>
<header class="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!--<ul class="navbar-nav mr-auto">
                 @can ('viewAny', \App\Models\User::class)
                 <li class="nav-item">
                     <a class="nav-link" href="/users">Usuarios</a>
                 </li>
                 @endcan
                 <li class="nav-item">
                     <a class="nav-link" href="/doctors">Doctores</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="/patients">Pacientes</a>
                 </li>

                 <li class="nav-item">
                     <a class="nav-link" href="/surveys">Cuestionarios</a>
                 </li>
             </ul>-->
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('login') }}">{{ __('Inicio de sesión') }}</a>
                </li>
                @endif

                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>


                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">



                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Fin de sesión') }}
                        </a>



                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </nav>

    <body>
        <div class="container-fluid">
            <div class="row justify-content-center custom-margin">
                <div class="col-sm-6 col-md-8">
                    <!-- Add bg-primary in form tag if want form background color-->
                    <!--text-white if want text color white-->
                    <div class="card-body-fluid">
                        <table>
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th class="text-center" scope="col"></th>
                                        <th class="text-center" scope="col"></th>
                                        <th class="text-center" scope="col"></th>
                                        <th class="text-center" scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td class="align-middle">
                                            <div class="estilo text-white float-left">Dandelion</div>
                                        </td>
                                        <td>
                                            <div class="card-header text-white">{{ __('Formulario de registro') }}</div>

                                            <div class="card-body">
                                                <form method="POST" action="{{ route('register') }}">
                                                    @csrf

                                                    <div class="form-group row">
                                                        <label for="name" class="col-md-4 col-form-label text-white text-md-right">{{ __('Nombre') }}</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                            @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="email" class="col-md-4 col-form-label text-white text-md-right">{{ __('E-Mail') }}</label>

                                                        <div class="col-md-6">
                                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                            @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password" class="col-md-4 col-form-label text-white text-md-right">{{ __('Contraseña') }}</label>

                                                        <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password-confirm" class="col-md-4 col-form-label text-white text-md-right">{{ __('Confirmar contraseña') }}</label>

                                                        <div class="col-md-6">
                                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-0">
                                                        <div class="col-md-6 offset-md-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                {{ __('Registrarse') }}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                        </td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <table>
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th class="text-center" scope="col"><i class="fas fa-align-center fa-4x fa-file-medical"></i></th>
                        <th class="text-center" scope="col"><i class="fas fa-4x fa-user-md"></th>
                        <th class="text-center" scope="col"><i class="fas fa-4x fa-poll"></i></th>
                        <th class="text-center" scope="col"><i class="fas fa-4x fa-chart-line"></i></th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td class="text-center text-white">Gestiona los roles de tu aplicación</td>
                        <td class="text-center text-white">Gestiona perfiles de doctores y pacientes</td>
                        <td class="text-center text-white">Gestiona cuestionarios</td>
                        <td class="text-center text-white">Analiza los resultados</td>
                    </tr>
                </tbody>
            </table>
</header>

</html>
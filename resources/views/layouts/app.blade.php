<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">




</head>

<header class="header mb-5">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background: linear-gradient(to right, #e58c8a, #eec0c6 );">

        <a class="navbar-brand ml-4" href="/home">
            <img src="../images/dandelion.PNG" class="rounded-circle border border-light" width="70" height="70" alt="dandelion">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                @can ('viewAny', \App\Models\User::class)


                <li class="nav-item">
                    <button type="button" class="btn btn-outline-light mr-3 mt-3 "><a class="nav-link text-light" href="/users">Usuarios</a></button>

                </li>
                @endcan
                @can ('viewAny', \App\Models\Patient::class)
                <li class="nav-item">
                    <button type="button" class="btn btn-outline-light mr-3 mt-3"><a class="nav-link text-light" href="/doctors">Doctores</a></button>

                </li>
                <li class="nav-item">

                    <button type="button" class="btn btn-outline-light mr-3 mt-3"><a class="nav-link text-light" href="/patients">Pacientes</a></button>
                </li>

                @endcan

                <li class="nav-item">
                    <button type="button" class="btn btn-outline-light mt-3 "><a class="nav-link text-light" href="/surveys">Cuestionarios</a></button>

                </li>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">

                <!-- Authentication Links -->
                @guest
                @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Inicio de sesión') }}</a>
                </li>
                @endif

                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link text-white dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>


                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">



                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Fin de sesión') }}
                        </a>



                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
            <!-- This menu is hidden in bigger devices with d-sm-none. 
           The sidebar isn't proper for smaller screens imo, so this dropdown menu can keep all the useful sidebar itens exclusively for smaller screens  -->
            </ul>
            @can ('viewAny', \App\Models\User::class)
            <li class="nav-item dropdown d-sm-block d-md-none">
                <a class="nav-link dropdown-toggle text-white" href="#" id="smallerscreenmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Panel Admin
                </a>
                <div class="dropdown-menu" aria-labelledby="smallerscreenmenu">
                    <a class="dropdown-item" href="/users">Usuarios</a>
                    <a class="dropdown-item" href="/doctors">Doctores</a>
                    <a class="dropdown-item" href="/patients">Pacientes</a>
                    <a class="dropdown-item" href="/surveys">Cuestionarios</a>
                </div>
            </li>
            @endcan

            <!-- Smaller devices menu END -->
            </ul>
        </div>
    </nav>
</header>

<body>
    @can ('viewAny', \App\Models\User::class)

    <div class="container-fluid" style="margin-top:120px">
        <div class="row">
            <nav class="col-xs-1 col-sm-1 col-md-2 d-none d-md-block sidebar">

                <div class="sidebar-sticky ">
                    <ul class=" nav flex-column">

                        <a href="#" data-toggle="collapse" aria-expanded="false" class="text-white list-group-item list-group-item-action flex-column align-items-start" style="background:#e58c8a;">
                            <div class="d-flex w-100 justify-content-start align-items-center">

                                <span class="  menu-collapsed"><i class=" fas fa-hospital-alt ml-3"> </i> Panel Admin </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>


                        <a href="#submenu1" data-toggle="collapse" aria-expanded="false" class=" text-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-start align-items-center">

                                <span class="menu-collapsed"><i class="fas fa-users ml-3"> </i> Usuarios </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>

                        <div id='submenu1' class="collapse sidebar-submenu">
                            <a href="/users" class="list-group-item list-group-item-action bg text-dark">
                                <span class="menu-collapsed">Listado Usuarios</span>
                            </a>

                        </div>
                        <a href="#submenu2" data-toggle="collapse" aria-expanded="false" class=" text-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-start align-items-center">

                                <span class="menu-collapsed"><i class="fas fa-user-md ml-3"> </i> Doctores </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <!-- Submenu content -->
                        <div id='submenu2' class="collapse sidebar-submenu">
                            <a href="/doctors" class="list-group-item list-group-item-action bg text-dark">

                                <span class="menu-collapsed">Listado Doctores</span>
                            </a>
                            <a href="/doctors/create" class="list-group-item list-group-item-action bg text-dark">
                                <span class="menu-collapsed">Nuevo Doctor</span>
                            </a>
                        </div>
                        <a href="#submenu3" data-toggle="collapse" aria-expanded="false" class=" text-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-start align-items-center">

                                <span class="menu-collapsed"><i class="fas fa-user-injured ml-3"> </i> Pacientes </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <!-- Submenu content -->
                        <div id='submenu3' class="collapse sidebar-submenu">
                            <a href="/patients" class="list-group-item list-group-item-action bg text-dark">

                                <span class="menu-collapsed">Listado Pacientes</span>
                            </a>
                            <a href="/patients/create" class="list-group-item list-group-item-action bg text-dark">
                                <span class="menu-collapsed">Nuevo Paciente</span>
                            </a>
                        </div>
                        <a href="#submenu4" data-toggle="collapse" aria-expanded="false" class=" text-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-start align-items-center">

                                <span class="menu-collapsed"> <i class="fas fa-poll ml-3"> </i> Cuestionarios </span>
                                <span class="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <!-- Submenu content -->
                        <div id='submenu4' class="collapse sidebar-submenu">
                            <a href="/surveys" class="  list-group-item list-group-item-action bg text-dark">

                                <span class="menu-collapsed">Listado Cuestionarios</span>
                            </a>
                            <a href="/surveys/create" class="list-group-item list-group-item-action bg text-dark">
                                <span class="menu-collapsed">Nuevo Cuestionario</span>
                            </a>
                            <a href="{{ route('export') }}" class="list-group-item list-group-item-action bg text-dark">
                                <span class="menu-collapsed">Exportar en excel</span>
                            </a>

                        </div>

                    </ul>
                    @endcan




                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">

                        <a class="d-flex align-items-center text-muted" href="#"></a>
                    </h6>

                </div>
            </nav>


            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">

                    @yield('content')


                </div>
            </main>
        </div>
    </div>

    <!-- Footer -->
    <footer class="page-footer text-white bg-dark font-small mdb-color pt-4">

        <!-- Footer Links -->
        <div class="container text-center text-md-left">

            <!-- Footer links -->


            <!-- Grid row -->
            <div class="row d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-7 col-lg-8">

                    <!--Copyright-->
                    <p class="text-center text-md-left">Año 2021@ Copyright:
                        <a href="#">
                            <strong> Dandelion </strong>
                        </a>
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-5 col-lg-4 ml-lg-0">

                    <!-- Social buttons -->
                    <div class="text-center text-md-right">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

    </footer>
    <!-- Footer -->
    @yield('footer')
</body>


</html>
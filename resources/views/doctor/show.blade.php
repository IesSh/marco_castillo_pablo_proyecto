@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card m-5">
                    <div class="card-header text-white mb-3"style="background:#e58c8a;">
                        <h1>Doctor nº {{$doctor->id}}</h1>
                    </div>
                <ul class="list-group">
                    <li class="list-group-item"><strong> DNI : </strong>
                        {{ $doctor->dni }}</li>
                    <li class="list-group-item"><strong> Nombre : </strong>
                        {{ $doctor->nombre }}</li>
                    <li class="list-group-item"><strong> Primer Apellido : </strong>
                        {{ $doctor->apellido1 }}</li>
                    <li class="list-group-item"><strong> Segundo Apellido : </strong>
                        {{ $doctor->apellido2}}</li>
                    <li class="list-group-item"><strong> Teléfono : </strong>
                        {{ $doctor->telefono }}</li>
                    <li class="list-group-item"><strong> Especialidad : </strong>
                            {{ $doctor->especialidad }}</li>
                </ul>


            </div>
        </div>
    </div>
</div>


@endsection
@extends('layouts.app')

<style>
    table {
        display: block;
        overflow-x: auto;
    }

    .static {
        position: absolute;
        background: #fff;
        

    }

    .first-col {
        padding-left: 120px !important;
    }
</style>

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card border-secondary m-5">
                <div class="class-header m-5">
                    <h3>
                        <i class="fas fa-user-md ml-5"> DOCTORES </i>
                        @can ('create', \App\Models\Doctor::class)
                        <a class="btn btn-secondary btn-sm ml-3" href="/doctors/create">Nuevo</a>
                        @endcan
                        <a class="btn btn-secondary btn-sm ml-3" href="{{ route('export') }}">Exportar cuestionarios</a>


                    </h3>
                </div>
            </div>



            <div class="card-header m-5 ">
                <form action="/doctors" method="get">
                    <th><input class="form-control" type="text" placeholder="ingresa solo el nombre por favor" name="nombre" value="{{$nombre}}"></th>
                    <th><input class="btn text-white mt-3" style="background:#e58c8a;" type="submit" value="Filtrar"></th>
                </form>

            </div>
            <table class="table table-hover table-responsive">
                
                    <thead>

                        <tr style="text-align:center">

                            <th class="static"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">DNI</button></th>
                            <th class="first-col" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Nombre</th>
                            <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Apellido1</th>
                            <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Apellido2</th>
                            <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Teléfono</th>
                            <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Especialidad</th>

                        </tr>
                </div>
            
                </thead>
    

        <tbody>
            @forelse ($doctors as $doctor)
            <tr >

                <td class="static">{{$doctor->dni}} </td>
                <td class="first-col">{{$doctor->nombre}} </td>
                <td>{{$doctor->apellido1}} </td>
                <td>{{$doctor->apellido2}} </td>
                <td>{{$doctor->telefono}} </td>
                <td>{{$doctor->especialidad}} </td>
                <td> <a class="btn-sm text-white" style="background:#e58c8a;" btn-sm" href="/doctors/{{$doctor->id}}"><i class="far fa-eye"></a></td>
                @can ('update', $doctor)
                <td> <a class="btn-sm text-white" style="background:#e58c8a;" btn-sm" href="/doctors/{{$doctor->id}}/edit"><i class="far fa-edit"></a></td>
                @endcan
                @can ('delete', $doctor)
                <td>
                    <form action="/doctors/{{$doctor->id}}" method="post">
                        @csrf

                        <input type="hidden" name="_method" value="DELETE">

                        <input class="btn btn-danger btn-sm" type="submit" value="Borrar">
                    </form>
                </td>
                @endcan

            </tr>
            @empty
            <tr>
                <td colspan="3">No hay doctores registrados con ese nombre</td>
            </tr>
            @endforelse
        </tbody>
        </table>
        {!! $doctors->links() !!}
    </div>
</div>
</div>


@endsection
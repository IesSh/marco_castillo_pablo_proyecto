@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
            <div class="class-header m-5">

                <h2 class="text-center"><i class="fas fa-address-card ml-3 mt-3 "> Añadir nuevo doctor </i></h2>
            </div>
            </div>
                
                <div class="card-body">

                    <form action="/doctors" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="dni"><strong>DNI</strong></label>
                            <input type="text" name="dni" value="{{ old('dni')}}" class="form-control" id="dni" aria-describedby="H" placeholder="Introduce el dni">
                            @error('dni')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="nombre"><strong>Nombre</strong></label>
                            <input type="text" name="nombre" value="{{ old('nombre')}}" class="form-control" id="nombre" aria-describedby="H" placeholder="Introduce el nombre">
                            @error('nombre')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="apellido1"><strong>Primer Apellido</strong></label>
                            <input type="text" name="apellido1" value="{{ old('apellido1')}}" class="form-control" id="apellido1" aria-describedby="H" placeholder="Introduce el primer apellido">
                            @error('apellido1')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="apellido2"><strong>Segundo Apellido</strong></label>
                            <input type="text" name="apellido2" value="{{ old('apellido2')}}" class="form-control" id="apellido2" aria-describedby="H" placeholder="Introduce el segundo apellido">
                            @error('apellido2')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="telefono"><strong>Teléfono</strong></label>
                            <input type="text" name="telefono" value="{{ old('telefono')}}" class="form-control" id="telefono" aria-describedby="H" placeholder="Introduce el teléfono">
                            @error('telefono')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="especialidad"><strong>Especialidad</strong></label>
                            <input type="text" name="especialidad" value="{{ old('especialidad')}}" class="form-control" id="especialidad" aria-describedby="H" placeholder="Introduce la especialidad">
                            @error('especialidad')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div>
                            <button type="submit" class="btn text-white"style="background:#e58c8a;">Añadir Doctor</button>
                        </div>
                    </form>
                    @if(count($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection
@extends('layouts.app')

@section('content')

<div class="container-fluid mb-5">
    <div class="row justify-content-center">
        <div class="col-md-10 mt-5">
        <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
            <div class="class-header m-5">
            <h2 class="text-center"><i class="fas fa-poll ml-3 mt-3"></i> Añadir nuevo cuestionario </h2>
            </div>
            </div>
            <form action="/surveys" method="post">
                @csrf

                <div class="form-group">
                    <label for="title"><strong>Título</strong> </label>
                    <input type="text" name="title" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Introduce el título">
                    <small id="title" class="form-text text-muted">Dale a tu cuestionario un título </small>
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror 
                </div>
                <div class="form-group">
                    <label for="description"><strong>Descripción</strong> </label>
                    <input type="text" name="description" class="form-control" id="description" aria-describedby="descriptionHelp" placeholder="Introduce una descripción">
                    <small id="description" class="form-text text-muted">Describe el propósito de tu cuestionario </small>
                    @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror 
                </div>

                <button type="submit" class="btn text-white"style="background:#e58c8a;">Añadir Cuestionario</button>
            </form>
        </div>
    </div>

</div>

<!--<div class="container">
    <div class="row">
        <table border="1" class="table" id="tablaEncuesta">
            <thead class="thead-dark">
                <tr>
                    <th>Pregunta</th>
                    <th>Respuesta</th>

                </tr>
            </thead>
            <tbody></tbody>
        </table><br>



    </div>
    <form>
        <div class="form-group">
            <label for="formGroupExampleInput"></label>
            <input type="text" class="form-control" id="pregunta" placeholder="Introduce la Pregunta">
        </div>
        
            
            <select class="custom-select my-1 mr-sm-2" id="respuesta">
                <option selected>Tipo de respuesta...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
    
    </form>
    <br>


    <button type="button" class="btn btn-primary" onclick="agregarFila()">Agregar</button>
    <button type="button" class="btn btn-danger" onclick="eliminarFila()">Eliminar</button>

</div>

</div>

<script>
    function agregarFila() {
        var pregunta = document.getElementById("pregunta").value;
        var respuesta = document.getElementById("respuesta").value;

        document.getElementById("tablaEncuesta").insertRow(-1).innerHTML =
        '<td>' + pregunta + '</td>'
        '<td>' + respuesta + '</td>'

    }

    function eliminarFila() {

        var table = document.getElementById("tablaEncuesta");
        var rowCount = table.rows.length;
        //console.log(rowCount);

        if (rowCount <= 1) alert('No se puede eliminar el encabezado');
        else table.deleteRow(rowCount - 1);
    }
</script>-->


@endsection
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center ">
        <div class="col-md-10">
            <div class="card text-white border-secondary m-5"style="background:#e58c8a;">
                <div class="class-header m-5">
                   
                        <h3 class="text-center"><i class="fas fa-poll"> Editar el cuestionario {{$survey->title}} </i></h3>
                    </div>
                </div>

                <form action="/surveys/{{$survey->id}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label ml-5"><strong>Título</strong></label>
                        <div class="col-sm-10 ml-5">
                            <input type="text" name="title" value="{{$survey->title}}" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="description" class=" col-sm-2 col-form-label ml-5"><strong>Descripción</strong></label>
                        <div class="col-sm-10 ml-5">
                            <input type="text" name="description" value="{{$survey->description}}" class="form-control">
                        </div>
                    </div>



                    <div>
                        <button type="submit" class="btn text-white ml-5 mb-5" style="background:#e58c8a;">Editar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('layouts.app')

/<!--<style>
    table {
        display: block;
        overflow-x: auto;
    }

    .static {
        position: absolute;
        background: #fff;
        

    }

    .first-col {
        padding-left: 120px !important;
    }

    
</style>-->

@section('content')
<div class="container-fluid mb-5">
    <div class="row justify-content-center">
        <div class="col-md-10">
        <div class="card border-secondary mt-5">
                <div class="class-header m-5">
                <h3>
                    <i class="fas fa-poll ml-5"> CUESTIONARIOS </i>
                    @can('create', \App\Models\Survey::class)

                    <a class="btn btn-secondary btn-sm ml-3" href="/surveys/create">Nuevo</a>
                    @endcan

                </h3>
            </div>
        </div>
            <div class="card-header bg-transparent m-5">
                

            
            <table class="table table-responsive table-hover">
                <thead>
                    <tr>
                        <th class="static" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Título</th>
                        <th class="first-col" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Descripción</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @forelse ($surveys as $survey)
                    <tr>
                        <td class="static">{{$survey->title}} </td>
                        <td class="first-col">{{$survey->description}} </td>
                        


                        @can('view', $survey)
                        <td> <a class="btn btn-sm  text-white" style="background:#e58c8a;" href="/surveys/{{$survey->id}}"><i class="far fa-eye"></a></td>
                        @endcan
                        <!--botón para que los pacientes puedan completar el cuestionario, usamos el slug método The Str::slug method generates a URL friendly "slug" from the given string:-->
                        <td> <a class="btn btn-success btn-sm " href="/questionnaires/{{$survey->id}}-{{Str::slug($survey->title)}}">Completar Cuestionario</a></td>

                        @can('update', $survey)
                        <td> <a class="btn btn-sm  text-white" style="background:#e58c8a;" href="/surveys/{{$survey->id}}/edit"><i class="far fa-edit"></a></td>
                        @endcan
                        @can('delete', $survey)
                        <td>
                            <form action="/surveys/{{$survey->id}}" method="post">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input class="btn btn-danger btn-sm float-right" type="submit" value="Borrar">
                        </td>
                        @endcan
                        </form>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">No hay cuestionarios registrados</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {!! $surveys->links() !!}
        </div>

    </div>
</div>
@endsection

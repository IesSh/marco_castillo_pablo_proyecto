@extends('layouts.app')

<style>
    table {
        display: block;
        overflow-x: auto;
    }

    .static {
        position: absolute;
        background: #fff;


    }

    .first-col {
        padding-left: 170px !important;
    }
</style>

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 ">
            <div class="card border-secondary m-5">
                <div class="class-header m-5">
                    <h3><i class="fas fa-users"> USUARIOS </i></h3>
                </div>
            </div>

            
            <table class="table table-hover table-responsive">
                <thead>
                    <tr>
                        <th class="static" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Nombre</th>
                        <th class="first-col" scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Email</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Password</th>
                        <th scope="col"><button type="button" class="btn text-black mr-3 mt-3"style="border: 1px solid black">Rol</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                    <tr>

                        <td class="static">{{$user->name}} </td>
                        <td class="first-col">{{$user->email}} </td>
                        <td>{{$user->password}} </td>
                        <td>{{$user->role_id}} </td>


                        
                        <td> <a class="btn btn-sm text-white" style="background:#e58c8a;" href="/users/{{$user->id}}/edit"><i class="far fa-edit"></i></a></td>

                    
                        <td>
                            <form action="/users/{{$user->id}}" method="post">
                                @csrf

                                <input type="hidden" name="_method" value="DELETE">

                                <input class="btn btn-danger btn-sm" type="submit" value="Borrar">
                            </form>
                        </td>
                    </tr>

                    @empty
                    <tr>
                        <td colspan="3">No hay usuarios registrados con ese nombre</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {!! $users->links() !!}
        </div>

    </div>
</div>
@endsection
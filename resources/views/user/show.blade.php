@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                    <div class="card-header text-white mb-3" style="background:#e58c8a;">
                        <h1>Usuario nº {{$user->id}}</h1>
                    </div>
                <ul class="list-group">
                    
                    <li class="list-group-item"><strong> Nombre : </strong>
                        {{ $user->name }}</li>
                    <li class="list-group-item"><strong> Email : </strong>
                        {{ $user->email }}</li>
                    <li class="list-group-item"><strong> Contraseña : </strong>
                        {{ $user->password}}</li>
                    <li class="list-group-item"><strong> Rol : </strong>
                        {{ $user->rol }}</li>
                    
                </ul>


            </div>
        </div>
    </div>
</div>


@endsection
<!doctype html>
<html lang="en">
  <head>
    <title>Laravel 8 Import Export Excel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
      <div class="container py-4">
          <div class="row">
              <div class="col-xl-12 text-right">
                  <a href="{{ route('export') }}" class="btn btn-primary">Export to Excel/CSV</a>
              </div>
          </div>

          <div class="card mt-4">
              <div class="card-header">
                <h5 class="card-title font-weight-bold">Export/Import Tutorial in Laravel 8</h5>
              </div>
              <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Contraseña</th>
                            <th>Rol</th>
                        </tr>
                    </thead>

                    <tbody>
                      @forelse ($users as $user)
                          <tr>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $user->password }}</td>
                              <td>{{ $user->role_id }}</td>
                          </tr>
                      @empty

                      @endforelse
                    </tbody>
                </table>
              </div>
          </div>
      </div>
  </body>
</html>
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('doctor_id')->default(1)->constrained()->onDelete('cascade');
            $table->string('dni',9)->unique();
            $table->string('nombre');
            $table->string('apellido1');
            $table->string('apellido2');
            $table->string('sexo');
            $table->string('fechaNacimiento');
            $table->string('direccion');
            $table->string('poblacion');
            $table->string('provincia');
            $table->integer('CP');
            $table->integer('telefono');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Patient;

class PatientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::create([
            'doctor_id' => '1',
            'dni' => '12345678Y',
            'nombre' => 'Pietro',
            'apellido1' => 'Di fiesta',
            'apellido2' => 'Anton',
            'sexo' => 'H',
            'fechaNacimiento' => '10-10-1978',
            'direccion' => 'calle apostol',
            'poblacion' => 'Reina',
            'provincia' => 'Terrer',
            'CP' => '31300',
            'telefono' => '622506984',
            
        ]);
        Patient::factory(20)->create();

    }
}

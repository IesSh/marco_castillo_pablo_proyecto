<?php

namespace Database\Seeders;

use App\Models\Survey;
use Illuminate\Database\Seeder;

class SurveysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Survey::create([
            'user_id'=>'1',
            'title' => 'Cáncer',
            'description' => 'Oncología'
                        
        ]);
    }
}

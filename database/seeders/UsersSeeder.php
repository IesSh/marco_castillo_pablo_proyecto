<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'id' => '1',            
            'name' => 'doctor',
            'email' => 'doctor@dws.es',
            'password' => bcrypt('secret'),
            'role_id'=>'1',
            
        ]);
    
        \DB::table('users')->insert([
            'id' => '2',            
            'name' => 'paciente',
            'email' => 'paciente@dws.es',
            'password' => bcrypt('secret'),
            'role_id'=>'2',
        
        ]);
    
        \DB::table('users')->insert([
            'id' => '3',            
            'name' => 'admin',
            'email' => 'admin@dws.es',
            'password' => bcrypt('secret'),
            'role_id'=>'3',
        
        ]);
        User::factory(20)->create();
    }
}

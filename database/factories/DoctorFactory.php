<?php

namespace Database\Factories;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\Factory;

class DoctorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Doctor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dni' => $this->faker->numberBetween(1,1000000),
            'nombre' => $this->faker->word(),
            'apellido1' => $this->faker->word(),
            'apellido2' => $this->faker->word(),
            'telefono' => $this->faker->numberBetween(1,100),
            'especialidad' => $this->faker->word(),
                ];
    }
}
